#/bin/sh
truncate -s 0 debian/tests/require
cat >> debian/tests/require <<EOF
#!/bin/sh
set -e
EOF
for i in lodash.*
do
  echo "nodejs -e \"require('$i');\"" >> debian/tests/require
done
